package EjercicioTCP;

import java.io.*;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

import static EjercicioTCP.ServidorTCP.semaforo;

public class ClienteTCP extends Thread{

    private static boolean estado = true;

    @Override
    public void run () {


        Scanner scn = new Scanner(System.in);
        Scanner sch = new Scanner(System.in);
        Scanner scl = new Scanner(System.in);


        System.out.println("\nCreando y conectando el socket stream cliente");
        try {
            Socket cliente = new Socket("localhost", 50005);

            //Stream de entrada de datos
            InputStream is = cliente.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            //Stream de salida de datos
            OutputStream os = cliente.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);


            System.out.println("Recibiendo mensaje del servidor:");
            String mensaje_string1 = dis.readUTF();
            System.out.println("Mensaje del servidor: " + mensaje_string1);

            System.out.println("Enviando el primer operando al servidor:");


            try{
                int mensaje = scn.nextInt();

                if (mensaje == 0) {
                    estado = false;
                    System.out.println("Adios");
                    dos.writeInt(mensaje);
                    return;
                }
                dos.writeInt(mensaje);
            }

            catch (InputMismatchException i){
                System.out.println("Valor no soportado, se asigna el valor 8");
                dos.writeInt(8);

            }



            System.out.println("Mensaje enviado");


            System.out.println("Recibiendo mensaje del servidor:");
            String mensaje_string2 = dis.readUTF();
            System.out.println("Mensaje del servidor: " + mensaje_string2);

            System.out.println("Enviando el segundo operando al servidor:");




            try{

                int mensaje2 = sch.nextInt();
                if (mensaje2 == 0) {
                    estado = false;
                    System.out.println("Adios");
                    dos.writeInt(mensaje2);
                    return;
                }
                dos.writeInt(mensaje2);


            }
            catch (InputMismatchException i){
                System.out.println("Valor no soportado, se asigna el valor 4");
                dos.writeInt(4);
            }



            System.out.println("Mensaje enviado");



            System.out.println("Recibiendo mensaje del servidor:");
            String mensaje_string3 = dis.readUTF();
            System.out.println("Mensaje del servidor: " + mensaje_string3);




            System.out.println("Enviando el operador al servidor:");

            String mensaje3 = scl.nextLine();

            if(Objects.equals(mensaje3, "*") || Objects.equals(mensaje3, "/") || Objects.equals(mensaje3, "+") || Objects.equals(mensaje3, "-")){
                dos.writeUTF(mensaje3);
            }
            else {
                System.out.println("Opcion invalida, se asignara el operador '+'");
                dos.writeUTF("+");

            }



            System.out.println("Operador enviado");


            System.out.println("Recibiendo mensaje del servidor:");
            String mensaje_string4 = dis.readUTF();
            System.out.println("Mensaje del servidor: " + mensaje_string4);



            dos.close();
            dis.close();
            cliente.close();
            System.out.println("Terminado");



        } catch (IOException e) {
            System.err.println("Error al crear el socket.");
            System.out.println(e.getMessage());

        }


    }
    public static boolean Apagar (){

        return estado;

    }





}

