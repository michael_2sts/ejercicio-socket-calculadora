package EjercicioTCP;

import EjercicioUDP.ServidorUDP;

import java.util.concurrent.Semaphore;

public class MainTCP {

    public static void main(String[] args) throws InterruptedException {


        //Cantidad de hilos que va a haber
        int hilos = 5;


        Semaphore s = new Semaphore(5); // Semáforo con valor inicial 1

        ServidorTCP server = new ServidorTCP(s,hilos);
        ClienteTCP[] c = new ClienteTCP[hilos];


        server.start(); // Iniciar el servidor


            for (int i = 0; i < c.length; i++) {

                c[i] = new ClienteTCP();
                c[i].start();
                c[i].join();

                if(!ClienteTCP.Apagar()){
                    break;
                }

            }

    }
}


