package EjercicioTCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Objects;
import java.util.concurrent.Semaphore;


public class ServidorTCP extends Thread{

    //el semaforo esta en el servidor
    static Semaphore semaforo;
    int hilos;

    public ServidorTCP(Semaphore semaforo, int hilos) {
        this.semaforo = semaforo;
        this.hilos = hilos;
    }


    @Override
    public void run (){

        int contador = 0;

        boolean seguir = true;

        while (seguir) {


            try {
                semaforo.acquire();
                contador++;

                //construir el objeto para que escuche por el puerto indicado
                try (ServerSocket servidor = new ServerSocket(50005)) {

                    //Espera bloqueado conexiones de clientes
                    //Cuando llegue una devolverá el accept un Socket
                    Socket cliente = servidor.accept();

                    System.out.println("Conexión establecida!");

                    //inicialización de los streams para entreda y salida de datos
                    DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                    DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());



                    salida.writeUTF("Dime el primer operando o '0' para salir");

                    int eleccion = entrada.readInt();

                        if(eleccion ==0){
                            servidor.close();
                            seguir = false;
                            break;
                        }

                    salida.writeUTF("Dime el segundo operando o '0' para salir");

                    int eleccion1 = entrada.readInt();

                        if(eleccion1 ==0){
                            servidor.close();
                            seguir = false;
                            break;
                        }

                    salida.writeUTF("Dime el operando [+,-,/,*]");

                    String eleccion3 = entrada.readUTF();

                    int soluccion;

                    if (Objects.equals(eleccion3, "*")) {
                        soluccion = eleccion * eleccion1;
                        salida.writeUTF("La soluccion es: " + soluccion);

                    } else if (Objects.equals(eleccion3, "/")) {
                        soluccion = eleccion / eleccion1;
                        salida.writeUTF("La soluccion es: " + soluccion);

                    } else if (Objects.equals(eleccion3, "+")) {
                        soluccion = eleccion + eleccion1;
                        salida.writeUTF("La soluccion es: " + soluccion);

                    } else if (Objects.equals(eleccion3, "-")) {
                        soluccion = eleccion - eleccion1;
                        salida.writeUTF("La soluccion es: " + soluccion);

                    }


                    System.out.println("Cerrando Streams y sockets...");

                    semaforo.release();
                    entrada.close();
                    salida.close();
                    cliente.close();

                    if (contador == hilos) {
                        servidor.close();
                        seguir = false;
                    }
                }


            } catch (IOException e) {
                System.err.println("Error al crear el socket");
                System.out.println(e.getMessage());
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

}


}
