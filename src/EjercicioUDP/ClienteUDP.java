package EjercicioUDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.InputMismatchException;
import java.util.Objects;
import java.util.Scanner;

public class ClienteUDP extends Thread {

    private static boolean estado = true;



    @Override
    public void run () {

        Scanner scl = new Scanner(System.in);

        byte[] buffer = new byte[1024];

        System.out.println("\nCreando y conectando el socket stream cliente");

        try {

            DatagramSocket datagramSocket = new DatagramSocket();
            InetAddress serverAddr = InetAddress.getByName("localhost");


            //Recepción de mensajes

            DatagramPacket paquete = new DatagramPacket(buffer, buffer.length);
            datagramSocket.receive(paquete);
            String mensaje1 = new String(paquete.getData(), StandardCharsets.UTF_8);
            System.out.println("Mensaje del servidor: " + mensaje1);

            System.out.println("Enviando el primer operando al servidor...");

            try {
                String mensaje = scl.nextLine();

                if (Integer.parseInt(mensaje) == 0) {
                    estado = false;
                    System.out.println("Adios");
                    DatagramPacket operando1 = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                    datagramSocket.send(operando1);
                    return;
                }
                DatagramPacket operando1 = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                datagramSocket.send(operando1);
            } catch (InputMismatchException i) {
                System.out.println("Valor no soportado, se asigna el valor 8");
                String operando_error = "8";
                DatagramPacket operando1 = new DatagramPacket(operando_error.getBytes(), operando_error.length(), serverAddr, 5555);
                datagramSocket.send(operando1);
            }


            System.out.println("Mensaje enviado");


            System.out.println("Recibiendo mensaje del servidor:");
            DatagramPacket paquete1 = new DatagramPacket(buffer, buffer.length);
            datagramSocket.receive(paquete1);
            String mensaje2 = new String(paquete1.getData(), StandardCharsets.UTF_8);
            System.out.println("Mensaje del servidor: " + mensaje2);

            System.out.println("Enviando el segundo operando al servidor:");

            try {
                String mensaje3 = scl.nextLine();

                if (Integer.parseInt(mensaje3) == 0) {
                    estado = false;
                    System.out.println("Adios");
                    DatagramPacket operando2 = new DatagramPacket(mensaje3.getBytes(), mensaje3.length(), serverAddr, 5555);
                    datagramSocket.send(operando2);
                    return;
                }
                DatagramPacket operando2 = new DatagramPacket(mensaje3.getBytes(), mensaje3.length(), serverAddr, 5555);
                datagramSocket.send(operando2);
            } catch (InputMismatchException i) {
                System.out.println("Valor no soportado, se asigna el valor 8");
                String operando_error = "8";
                DatagramPacket operando2 = new DatagramPacket(operando_error.getBytes(), operando_error.length(), serverAddr, 5555);
                datagramSocket.send(operando2);
            }

            System.out.println("Mensaje enviado");

            System.out.println("Recibiendo mensaje del servidor:");
            DatagramPacket paquete3 = new DatagramPacket(buffer, buffer.length);
            datagramSocket.receive(paquete3);
            String mensaje4 = new String(paquete3.getData(), StandardCharsets.UTF_8);
            System.out.println("Mensaje del servidor: " + mensaje4);


            System.out.println("Enviando el operador al servidor:");

            String mensaje5 = scl.nextLine();

            if (Objects.equals(mensaje5, "*") || Objects.equals(mensaje5, "/") || Objects.equals(mensaje5, "+") || Objects.equals(mensaje5, "-")) {
                DatagramPacket operando3 = new DatagramPacket(mensaje5.getBytes(), mensaje5.length(), serverAddr, 5555);
                datagramSocket.send(operando3);
            } else {
                System.out.println("Opcion invalida, se asignara el operador '+'");
                String operando_error = "+";
                DatagramPacket operando3 = new DatagramPacket(operando_error.getBytes(), operando_error.length(), serverAddr, 5555);
                datagramSocket.send(operando3);

            }


            System.out.println("Operador enviado");

            System.out.println("Recibiendo mensaje del servidor:");
            DatagramPacket paquete4 = new DatagramPacket(buffer, buffer.length);
            datagramSocket.receive(paquete4);
            String mensaje6 = new String(paquete4.getData(), StandardCharsets.UTF_8);
            System.out.println("Mensaje del servidor: " + mensaje6);


            System.out.println("Terminado");



        }catch (IOException e) {
            System.err.println("Error al crear el socket.");
            System.out.println(e.getMessage());
        }
    }

    public static boolean Apagar (){
        return estado;
    }
}
