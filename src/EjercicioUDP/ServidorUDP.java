package EjercicioUDP;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.concurrent.Semaphore;

public class ServidorUDP extends Thread {


    //el semaforo esta en el servidor
    static Semaphore semaforo;
    static int hilos;

    public ServidorUDP(Semaphore semaforo, int hilos) {
        this.semaforo = semaforo;
        this.hilos = hilos;
    }




    @Override
    public void run () {

        int contador = 0;

        boolean seguir = true;


        byte[] buffer = new byte[1024];


        while (seguir) {

            try {
                semaforo.acquire();
                contador++;

                try (ServerSocket servidor = new ServerSocket(50005)) {

                    InetSocketAddress addr = new InetSocketAddress("127.0.0.1", 5555);
                    DatagramSocket socket = new DatagramSocket(addr);
                    DatagramSocket datagramSocket = new DatagramSocket();
                    InetAddress serverAddr = InetAddress.getByName("localhost");
                    System.out.println("Servidor conectado");


                    String mensaje = "Dime el primer operando o '0' para salir";

                    DatagramPacket paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                    datagramSocket.send(paquete);


                    //Recepción de mensajes

                    paquete = new DatagramPacket(buffer, buffer.length);
                    socket.receive(paquete);

                    String mensaje1 = new String(paquete.getData(), StandardCharsets.UTF_8);
                    int numero1 = Integer.parseInt(mensaje1);


                    if (numero1 == 0) {
                        servidor.close();
                        seguir = false;
                        break;
                    }

                    mensaje = "Dime el segundo operando o '0' para salir";

                    paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                    datagramSocket.send(paquete);

                    String mensaje2 = new String(paquete.getData(), StandardCharsets.UTF_8);
                    int numero2 = Integer.parseInt(mensaje2);


                    if (numero2 == 0) {
                        servidor.close();
                        seguir = false;
                        break;
                    }

                    mensaje = "Dime el operando [+,-,/,*]";

                    paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                    datagramSocket.send(paquete);


                    paquete = new DatagramPacket(buffer, buffer.length);
                    socket.receive(paquete);

                    String mensaje3 = new String(paquete.getData(), StandardCharsets.UTF_8);

                    int soluccion;

                        if (Objects.equals(mensaje3, "*")) {
                            soluccion = numero1 * numero2;
                            mensaje = "La soluccion es: " + soluccion;
                            paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                            datagramSocket.send(paquete);

                        } else if (Objects.equals(mensaje3, "/")) {
                            soluccion = numero1 / numero2;
                            mensaje = "La soluccion es: " + soluccion;
                            paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                            datagramSocket.send(paquete);

                        } else if (Objects.equals(mensaje3, "+")) {
                            soluccion = numero1 + numero2;
                            mensaje = "La soluccion es: " + soluccion;
                            paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                            datagramSocket.send(paquete);

                        } else if (Objects.equals(mensaje3, "-")) {
                            soluccion = numero1 - numero2;
                            mensaje = "La soluccion es: " + soluccion;
                            paquete = new DatagramPacket(mensaje.getBytes(), mensaje.length(), serverAddr, 5555);
                            datagramSocket.send(paquete);
                        }

                    System.out.println("Cerrando Streams y sockets...");

                    semaforo.release();

                        if (contador == hilos) {
                            seguir = false;
                        }


                } catch (IOException e) {
                    System.err.println("Error al crear el socket");
                    System.out.println(e.getMessage());


                }

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }


}










